<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::middleware(['auth'])->group(function () {
	Route::get('category/create'		, 'CategoryController@create');
	Route::get('category/view'			, 'CategoryController@view')->name('categories');
	Route::get('category/update/{id}'	, 'CategoryController@update');
	Route::get('category/delete/{id}'	, 'CategoryController@delete');

	Route::post('category/save'		, 'CategoryController@save');
	Route::post('category/update'	, 'CategoryController@update');
});

Route::get('/home', 'HomeController@index')->name('home');
