<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Category;

class CategoryController extends Controller
{
    public function create()
    {
        return view('category.create');
    }

    public function view()
    {
        $categories = Category::paginate(5);
        return view('category.view', compact('categories'));
    }
    
    public function save(Request $request)
    {
    	$input                                  = $request->all();
        $SubCategoryController                  = new SubCategoryController;
        $CategorySubCategoryMatrixController    = new CategorySubCategoryMatrixController;
        $input['path']                          = $request->file('image')->store('category');
        $category                               = Category::create(['title' => $input['title'], 'image' => $input['path']]);

        foreach ($input['sub_category'] as $key => $sub_category) {
            $sub_category = $SubCategoryController->update_or_create($sub_category, $input['information'][$key]);
            $CategorySubCategoryMatrixController->create($category->id, $sub_category->id);
        }
        return  redirect('category/view')->with(['status' => 'Category created successfully.']);
    }

    public function update(Request $request , $id = null)
    {
        $input = $request->all();
        if ($request->isMethod('post')) {
            $SubCategoryController                  = new SubCategoryController;
            $CategorySubCategoryMatrixController    = new CategorySubCategoryMatrixController;

            $category           = Category::find($input['id']);
            $category->title    = $input['title'];
            if ($request->hasFile('image')) {
                $input['path']      = $request->file('image')->store('category');
                $category->image    = $input['path'];
            }
            $category->save();
            $sub_category_ids       = $category->sub_categories()->get()->pluck('id')->toArray();
            $delete_sub_category    = array_diff($sub_category_ids, $input['sub_category_id']);
            $CategorySubCategoryMatrixController->delete($delete_sub_category);

            foreach ($input['sub_category']??[] as $key => $sub_category) {
                $sub_category = $SubCategoryController->update_or_create($sub_category, $input['information'][$key]);
                $CategorySubCategoryMatrixController->create($category->id, $sub_category->id);
            }
            return  redirect()->back()->with(['status' => 'Category updated successfully.']);
        } else {
            $output['category'] = Category::find($id)->with('sub_categories')->get();
            return view('category.update', $output);
        }
    }

    public function delete($id)
    {
        Category::find($id)->delete();
        return  redirect('category/view')->with(['status' => 'Category deleted successfully.']);
    }
}
