<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\CategorySubCategoryMatrix;

class CategorySubCategoryMatrixController extends Controller
{
    public function create($category_id, $sub_category_id)
    {
    	return CategorySubCategoryMatrix::updateOrCreate(['category_id' => $category_id, 'sub_category_id' => $sub_category_id], []);
    }

    public function delete($ids)
    {
    	return CategorySubCategoryMatrix::destroy($ids);
    }
}
