<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\SubCategory;

class SubCategoryController extends Controller
{
    public function update_or_create($sub_category, $information)
    {
    	return SubCategory::updateOrCreate(['title' => $sub_category], ['information' => $information]);
    }
}
