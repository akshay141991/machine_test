<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategorySubCategoryMatrix extends Model
{

	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'category_sub_category_matrix';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'category_id',
		'sub_category_id'
	];
}
