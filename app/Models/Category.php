<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'category';
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'title',
		'image'
	];

    // relation with Sub Category -> M to M
    public function sub_categories()
    {
        return $this->belongsToMany(SubCategory::class, 'category_sub_category_matrix', 'category_id', 'sub_category_id');
    }
}
