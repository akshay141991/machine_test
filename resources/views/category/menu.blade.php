@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-sm-3 border border-dark bg-light">
				<ul class="nav flex-column">
					<li class="nav-item border-bottom">
						<a class="nav-link active" href="{{ url('category/view') }}">View</a>
					</li>
					<li class="nav-item border-bottom">
						<a class="nav-link" href="{{ url('category/create') }}">Create / Edit</a>
					</li>
				</ul>
			</div>
			<div class="col-sm-9">
            	@yield('form')
			</div>
		</div>
	</div>
@endsection