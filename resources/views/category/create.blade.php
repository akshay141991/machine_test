@extends('category.menu')

@section('form')
	<h2>Create Category</h2>
	<div class="row">
		<div class="col-md-9">
			<form action="{{ url('category/save') }}" method="post" id="create-category" enctype="multipart/form-data">
				@csrf
				<div class="form-group">
					<label>Title</label>
					<input type="text" class="form-control" name="title">
				</div>
				<div class="form-group">
					<label>Image</label>
					<input type="file" class="form-control-file" name="image">
				</div>
				<div class="template">
					<div class="sub-category">
						<div class="form-group">
				            <label>Sub Category</label>
				            <input type="text" class="form-control" name="sub_category[]">
				        </div>
						<div class="form-group">
							<label>Information</label>
							<textarea class="form-control" rows="3" name="information[]"></textarea>
				            <p class="text-right"><a href="javascript:;" class="delete-sub-category">Delete</a></p>
						</div>
					</div>
				</div>
				<div class="form-group">
					<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#sub-category">Add Sub Category</button>
				</div>
				<button type="submit" class="btn btn-primary">Save</button>
			</form>
			<div class="modal fade" id="sub-category" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title">New Sub Category</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<label for="recipient-name" class="col-form-label">Sub Category</label>
								<input type="text" class="form-control title">
							</div>
							<div class="form-group">
								<label for="message-text" class="col-form-label">Information</label>
								<textarea class="form-control information"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							<button type="button" class="btn btn-primary add-sub-category">Add Sub Category</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
