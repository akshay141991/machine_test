@extends('category.menu')

@section('form')
	<h2>View Category</h2>
	@if (session('status'))
		<div class="alert alert-success">
			{{ session('status') }}
		</div>
	@endif
	<table class="table table-hover table-bordered table-striped">
		<thead>
			<tr>
				<th scope="col">#</th>
				<th scope="col">Title</th>
				<th scope="col">Image</th>
				<th scope="col">Action</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($categories as $category)
				<tr>
					<th scope="row">
						{{ $category->id }}
					</th>
					<td>
						{{ $category->title }}
					</td>
					<td>
						<img class="img-thumbnail" src="{{ asset('storage/'.$category->image) }}" alt="Category Image">
					</td>
					<td>
						<ul class="nav justify-content-end">
							<li class="nav-item">
								<a class="btn btn-primary" href="{{ url('category/update', $category->id) }}">Update</a>
							</li>
							<li class="nav-item">
								<a class="btn btn-danger" href="{{ url('category/delete', $category->id) }}">Delete</a>
							</li>
						</ul>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
	{{ $categories->links() }}
@endsection
