/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});

let $template;

$(document).ready(function() {
	if ($('#create-category').length) {
		$template = $('#create-category .template').html();
		console.log($template);
		console.log($($template).find('input'));
		$('#create-category .template').html('');
	}
});

$('body').on('click', '#sub-category .add-sub-category', function(event) {
	let title 		= $('#sub-category').find('.title').val();
	let information = $('#sub-category').find('.information').val();
	let template 	= $($template);
	if (check_unique_sub_category(title) !== -1) {
		$('#sub-category').find('.modal-body').prepend('<div class="alert alert-danger" role="alert">Duplicate Sub Category</div>');
		return;
	}else{
		$('#sub-category').find('.modal-body .alert').remove();
	}

	$('#sub-category').find('.title').val('');
	$('#sub-category').find('.information').val('');
	$('#sub-category').modal('hide');
	
	template.find('input').val(title).attr('readonly', 'true');
	template.find('textarea').val(information).attr('readonly', 'true');

	$('#create-category .template').append(template);
});

$('body').on('click', '.delete-sub-category', function(event) {
	event.preventDefault();
	$(this).parents('.sub-category').remove();
});

function check_unique_sub_category(title) {
	let categories = [];
	$('.sub-category').each(function(index, el) {
		categories.push($(this).find('input').val());
	});
	console.log(categories);
	return $.inArray(title, categories);
}
