<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategorySubCategoryMatrixTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_sub_category_matrix', function (Blueprint $table) {
            $table->bigIncrements('id');
            
            $table->bigInteger('category_id')
                    ->unsigned()
                    ->index();
            
            $table->bigInteger('sub_category_id')
                    ->unsigned()
                    ->index();

            $table->timestamps();

            $table->unique(['category_id', 'sub_category_id']);

            $table->foreign('category_id')
                ->references('id')
                ->on('category')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');

            $table->foreign('sub_category_id')
                ->references('id')
                ->on('sub_category')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_sub_category_matrix');
    }
}
